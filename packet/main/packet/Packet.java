package main.packet;

import java.io.Serializable;

import main.packet.chat.Chat;
import main.packet.request.HostMatch;
import main.packet.request.JoinMatch;
import main.packet.request.JoinServer;
import main.packet.request.Request;
import main.packet.request.Request.REQ_TYPE;
import main.packet.response.GameStart;
import main.packet.response.MatchDetail;
import main.packet.response.PlayerDetail;
import main.packet.response.Response;

public abstract class Packet implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private final String senderInetAddress;
	private final String receiverInetAddress;
	private final int senderPort;
	private final int receiverPort;
	
	private final TYPE type;
	private final SCOPE scope;	
	
	public Packet(TYPE type, SCOPE scope, String senderInetAddress, String receiverInetAddress, 
			int senderPort, int receiverPort) {
		super();
		this.senderInetAddress = senderInetAddress;
		this.receiverInetAddress = receiverInetAddress;
		this.senderPort = senderPort;
		this.receiverPort = receiverPort;
		this.type = type;
		this.scope = scope;
	}

	public TYPE getType() {
		return type;
	}

	public SCOPE getScope() {
		return scope;
	}
	
	public String getSenderAddress(){
		return senderInetAddress;
	}
	
	public String getReceiverAddress(){
		return receiverInetAddress;
	}
	
	public int getSenderPort(){
		return senderPort;
	}
	
	public int getReceiverPort(){
		return receiverPort;
	}

	
	public static final class Builder{
		public static final class cht{
			public static Packet chat(SCOPE scope, String from, String message, String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Chat(scope, from, message, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
		}
		
		public static final class req{
			public static Packet joinServer(String nameString, String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new JoinServer(nameString, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet hostMatch(String matchName, String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new HostMatch(matchName, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet joinMatch(int matchId, String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new JoinMatch(matchId, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet leaveMatch(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Request(SCOPE.ROOM, Request.REQ_TYPE.LEAVE_MATCH, 
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet changeReady(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Request(SCOPE.ROOM, Request.REQ_TYPE.CHANGE_READY, 
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet startGame(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Request(SCOPE.ROOM, Request.REQ_TYPE.START_GAME, 
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet refreshRoom(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Request(SCOPE.ROOM, Request.REQ_TYPE.REFRESH_ROOMS, 
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
		}
		
		public static final class res{
			public static Packet successServerJoin(String playerName, String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new main.packet.response.JoinServer(playerName, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet successJoinMatch(int playerID, int matchId, String matchName, int mapId, int hostId, 
					String hostName, int numOfPlayers,
					String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort){
				return new main.packet.response.JoinMatch(playerID, REQ_TYPE.JOIN_MATCH, Response.Success.JOIN_MATCH,
						matchId, matchName, mapId, hostId, hostName, numOfPlayers,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet successHostMatch(int playerID, int matchId, String matchName, int mapId, int hostId, 
					String hostName, int numOfPlayers, 
					String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort){
				return new main.packet.response.JoinMatch(playerID, REQ_TYPE.HOST_MATCH, Response.Success.HOST_MATCH,
						matchId, matchName, mapId, hostId, hostName, numOfPlayers,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet refreshListMatch(int matchId, String matchName, int mapId, int hostId, 
					String hostName, int numOfPlayers, boolean created, 
					String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort){
				return new MatchDetail(SCOPE.GLOBAL, REQ_TYPE.REFRESH_ROOMS, Response.Success.HOST_MATCH,
						matchId, matchName, mapId, hostId, hostName, numOfPlayers, created,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet startGame(int matchId, String matchName, int mapId, int hostId, 
					String hostName, int numOfPlayers, boolean created, String udpServerInet, int updServerPort,
					String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort){
				return new GameStart(matchId, matchName, mapId, hostId, hostName, numOfPlayers, created,
						udpServerInet, updServerPort,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet playerDetail(int matchId, String matchName, int mapId, int hostId, 
					String hostName, int numOfPlayers, boolean left, int mapLocation,
					int playerId, String playerName, boolean ready, boolean started,
					String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort){
				return new PlayerDetail(REQ_TYPE.JOIN_MATCH, Response.Success.STATE_CHANGE,
						matchId, matchName, mapId, hostId, hostName, numOfPlayers,
						playerId, playerName, ready, started, left, mapLocation,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet errJoinServerNameExists(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Response(SCOPE.GLOBAL, REQ_TYPE.JOIN_SERVER, false, Response.Error.NAME_EXIST,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet errJoinMatchFull(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Response(SCOPE.GLOBAL, REQ_TYPE.JOIN_MATCH, false, Response.Error.MATCH_FULL,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet errJoinMatchDNE(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Response(SCOPE.GLOBAL, REQ_TYPE.JOIN_MATCH, false, Response.Error.MATCH_DNE,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet errStartGameNotHost(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Response(SCOPE.ROOM, REQ_TYPE.START_GAME, false, Response.Error.START_NOTHOST,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
			
			public static Packet errStartGameNotEveryoneReady(String senderInetAddress, String receiverInetAddress, 
					int senderPort, int receiverPort){
				return new Response(SCOPE.ROOM, REQ_TYPE.START_GAME, false, Response.Error.START_NOTEVERYONEREADY,
						senderInetAddress, receiverInetAddress, senderPort, receiverPort);
			}
		}
	}
	
	
	
	
	
	public enum TYPE{
		CHAT, REQUEST, RESPONSE
	}
	
	public enum SCOPE{
		GLOBAL, ROOM
	}
}
