package main.packet.request;

import main.packet.Packet;

public class JoinServer extends Request{
	private static final long serialVersionUID = 1L;

	private final String playerName;
	
	public JoinServer(String playerName, String senderInetAddress, String receiverInetAddress,
			int senderPort, int receiverPort) {
		super(Packet.SCOPE.GLOBAL, Request.REQ_TYPE.JOIN_SERVER, 
				senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		
		this.playerName = playerName;
	}

	public String getPlayerName() {
		return playerName;
	}

}
