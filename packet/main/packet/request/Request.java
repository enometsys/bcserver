package main.packet.request;

import main.packet.Packet;

public class Request extends Packet{
	private static final long serialVersionUID = 1L;
	
	private final REQ_TYPE reqType;

	public Request(SCOPE scope, REQ_TYPE reqType, String senderInetAddress, String receiverInetAddress, int senderPort,
			int receiverPort) {
		super(Packet.TYPE.REQUEST, scope, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		
		this.reqType = reqType;
	}

	
	public REQ_TYPE getReqType() {
		return reqType;
	}


	public enum REQ_TYPE{
		JOIN_SERVER,
		JOIN_MATCH,
		LEAVE_MATCH,
		HOST_MATCH,
		CHANGE_READY,
		START_GAME,
		REFRESH_ROOMS
	}
}
