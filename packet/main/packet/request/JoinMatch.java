package main.packet.request;

import main.packet.Packet;

public class JoinMatch extends Request{
	private static final long serialVersionUID = 1L;
	
	private final int match_id;

	public JoinMatch(int match_id, String senderInetAddress, String receiverInetAddress,
			int senderPort, int receiverPort) {
		super(Packet.SCOPE.GLOBAL, Request.REQ_TYPE.JOIN_MATCH, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		
		this.match_id = match_id;
	}

	public int getMatch_id() {
		return match_id;
	}

}
