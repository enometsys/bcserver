package main.packet.request;

import main.packet.Packet;

public class HostMatch extends Request{
	private static final long serialVersionUID = 1L;
	
	private final String matchName;

	public HostMatch(String matchName, String senderInetAddress, String receiverInetAddress,
			int senderPort, int receiverPort) {
		super(Packet.SCOPE.GLOBAL, Request.REQ_TYPE.HOST_MATCH, senderInetAddress, receiverInetAddress, senderPort, receiverPort);

		this.matchName = matchName;
	}

	public String getMatchName() {
		return matchName;
	}

}
