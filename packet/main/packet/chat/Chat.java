package main.packet.chat;

import main.packet.Packet;

public class Chat extends Packet{
	private static final long serialVersionUID = 1L;
	
	private final String from;
	private final String message;

	public Chat(SCOPE scope, String from, String message, String senderInetAddress, String receiverInetAddress, 
			int senderPort, int receiverPort) {
		super(Packet.TYPE.CHAT, scope, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		
		this.from = from;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public String getFrom() {
		return from;
	}

}
