package main.packet.response;

import main.packet.request.Request.REQ_TYPE;

public class GameStart extends MatchDetail{
	private static final long serialVersionUID = 1L;
	
	private final String udpServerInet;
	private final int updServerPort;
	
	public GameStart(int matchId, String matchName, int mapId, int hostId, String hostName, int numOfPlayers, boolean created,
			String udpServerInet, int updServerPort,
			String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort) {
		super(SCOPE.ROOM, REQ_TYPE.START_GAME, Response.Success.START_GAME, 
				matchId, matchName, mapId, hostId, hostName, numOfPlayers, created,
				senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		
		this.udpServerInet = udpServerInet;
		this.updServerPort = updServerPort;
	}

	public String getUdpServerInet() {
		return udpServerInet;
	}

	public int getUpdServerPort() {
		return updServerPort;
	}
	
}
