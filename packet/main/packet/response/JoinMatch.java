package main.packet.response;

import main.packet.request.Request.REQ_TYPE;

public class JoinMatch extends MatchDetail{
	private static final long serialVersionUID = 1L;
	
	private final int playerId;

	public JoinMatch(int playerId, REQ_TYPE reqType, String message, int matchId, String matchName, int mapId,
			int hostId, String hostName, int numOfPlayers, String senderInetAddress,
			String receiverInetAddress, int senderPort, int receiverPort) {
		super(SCOPE.GLOBAL, reqType, message, matchId, matchName, mapId, hostId, hostName, numOfPlayers, true,
				senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		this.playerId = playerId;
	}

	public int getPlayerId() {
		return playerId;
	}
	
}
