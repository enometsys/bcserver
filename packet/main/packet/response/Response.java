package main.packet.response;

import main.packet.Packet;
import main.packet.request.Request.REQ_TYPE;

public class Response extends Packet{
	private static final long serialVersionUID = 1L;
	
	private final REQ_TYPE reqType;
	private final boolean success;
	private final String message;

	public Response(SCOPE scope, REQ_TYPE reqType, boolean success, String message, String senderInetAddress, String receiverInetAddress, int senderPort,
			int receiverPort) {
		super(Packet.TYPE.RESPONSE, scope, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		
		this.reqType = reqType;
		this.success = success;
		this.message = message;
	}

	public REQ_TYPE getReqType() {
		return reqType;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}
	
	public static interface Success{
		public static final String JOIN_SERVER = "Successfully joined server!";
		public static final String START_GAME = "Match started!";
		public static final String JOIN_MATCH = "Match joined!";
		public static final String HOST_MATCH = "Successfully hosted match!";
		public static final String STATE_CHANGE = "Player State changed!";
	}
	
	public static interface Error{
		public static final String NAME_EXIST = "Name already exists in the server!";
		public static final String MATCH_FULL = "Match already full!";
		public static final String MATCH_DNE = "Match Already started or DNE!";
		public static final String START_NOTEVERYONEREADY = "Not everyone is ready!";
		public static final String START_NOTHOST = "Not host!";
	}
}
