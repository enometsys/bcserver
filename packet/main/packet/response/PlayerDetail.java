package main.packet.response;

import main.packet.request.Request.REQ_TYPE;

public class PlayerDetail extends MatchDetail{
	private static final long serialVersionUID = 1L;
	
	private final int playerId;
	private final String playerName;
	private final boolean ready;
	private final boolean started;
	private final int mapLocation;
	
	private final boolean left;
	
	public PlayerDetail(REQ_TYPE reqType, String message, 
			int matchId, String matchName, int mapId, int hostId, String hostName, int numOfPlayers,
			int playerId, String playerName, boolean ready, boolean started, boolean left, int mapLocation,
			String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort) {
		super(SCOPE.ROOM, reqType, message, matchId, matchName, mapId, hostId, hostName, numOfPlayers, true,
				senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		
		this.playerId = playerId;
		this.playerName = playerName;
		this.ready = ready;
		this.started = started;
		this.left = left;
		this.mapLocation = mapLocation;
	}

	public int getPlayerId() {
		return playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public boolean isReady() {
		return ready;
	}

	public boolean isStarted() {
		return started;
	}

	public boolean isLeft() {
		return left;
	}

	public int getMapLocation() {
		return mapLocation;
	}
	
	public boolean isHost(){
		return (getPlayerId() == super.getHostId());
	}
	
}
