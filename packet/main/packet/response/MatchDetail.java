package main.packet.response;

import main.packet.request.Request.REQ_TYPE;

public class MatchDetail extends Response{
	private static final long serialVersionUID = 1L;

	private final int matchId;
	private final String matchName;
	private final int mapId;
	private final int hostId;
	private final String hostName;
	
	//for refresh list
	private final int numOfPlayers;
	private final boolean created;
	
	public MatchDetail(SCOPE scope, REQ_TYPE reqType, String message, 
			int matchId, String matchName, int mapId, int hostId, String hostName, int numOfPlayers, boolean created, 
			String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort) {
		super(scope, reqType, true, message, senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		
		this.matchId = matchId;
		this.matchName = matchName;
		this.mapId = mapId;
		this.hostId = hostId;
		this.hostName = hostName;
		this.numOfPlayers = numOfPlayers;
		this.created = created;
	}
	
	public int getMatchId() {
		return matchId;
	}
	public String getMatchName() {
		return matchName;
	}
	public int getMapId() {
		return mapId;
	}
	public int getHostId() {
		return hostId;
	}
	public int getNumOfPlayers() {
		return numOfPlayers;
	}
	public boolean isCreated() {
		return created;
	}

	public String getHostName() {
		return hostName;
	}
}
