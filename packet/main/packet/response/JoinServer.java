package main.packet.response;

import main.packet.request.Request.REQ_TYPE;

public class JoinServer extends Response{
	private static final long serialVersionUID = 1L;
	
	private final String playerName;

	public JoinServer(String playerName, String senderInetAddress, String receiverInetAddress, int senderPort, int receiverPort) {
		super(SCOPE.GLOBAL, REQ_TYPE.JOIN_SERVER, true, Response.Success.JOIN_SERVER, 
				senderInetAddress, receiverInetAddress, senderPort, receiverPort);
		this.playerName = playerName;
	}

	public String getPlayerName() {
		return playerName;
	}
}
