package main.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import main.Main;
import main.ServerConnection;
import main.match.MatchConnection;
import main.packet.Packet;
import main.packet.Packet.SCOPE;
import main.packet.Packet.TYPE;
import main.packet.chat.Chat;
import main.packet.request.Request;
import main.packet.request.Request.REQ_TYPE;
import main.packet.response.JoinServer;
import main.packet.response.MatchDetail;
import main.packet.response.Response;

public class ClientHandler extends Thread implements Connection{
	private static final String TAG = ClientHandler.class.getSimpleName();	//For debugging
	
	private ServerConnection serverConnection;								//connection to server
	private MatchConnection matchConnection;								//connection to match, not in a match if null
	private Socket socket;													//Player socket
	
	private ObjectOutputStream out;											//outputStream
	private ObjectInputStream in;											//inputSteam
	
	private boolean connected;												//for stopping infinite loop
	
	/**
	 * PLAYER DETAILS:
	 * 
	 * player name
	 */
	private String playerName;
	
	
	/**
	 * CLIENT HANDLER CONSTRUCTOR
	 * sets server connection to @param main
	 * initializing: object (in/out)put stream of @param socket
	 */
	public ClientHandler(ServerConnection main, Socket socket) {
		this.serverConnection = main;
		this.socket = socket;
		
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, "failed init out");
		}
		
		try {
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, "failed init in");
		}
		
		Main.log.d(TAG, "constructed");
	}

	/**
	 * RUN
	 * start receiving packets
	 * leave match (if in one) and server before ending thread
	 */
	@Override
	public void run() {
		
		startReceiving();
		
		//if in match, leave match
		if (matchConnection != null){
			Main.log.i(TAG, getPlayerName() + " in match " + matchConnection.getMatchName() + " leaving match...");
			
			matchConnection.leaveMatch(this);
		}
		
		serverConnection.leaveServer(this);
	}
	
	/**
	 * IS NAME EXIST
	 * if response from initializing name,
	 * check @param pckt if accepted/rejected
	 * @return if accepted/rejected
	 */
	private boolean isNameExist(Packet pckt) {
		if (pckt != null){
			if (pckt.getType() == TYPE.RESPONSE){
				if(((Response) pckt).getReqType() == REQ_TYPE.JOIN_SERVER)
					return serverConnection.isNameExist(((JoinServer) pckt).getPlayerName());
			}
		}
		return false;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public MatchConnection getMatch(){
		return matchConnection;
	}
	
	
	
	/**
	 *  SOCKET CONNECTION -------------------------------------------------
	 */
	@Override
	public void startReceiving() {
		connected = true;
		while(isConnected()){
			try {
				receivePacket((Packet)in.readObject());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				Main.log.e(TAG, "startReceiving()", e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				Main.log.e(TAG, "startReceiving()", e.getMessage());
				try {
					in = new ObjectInputStream(socket.getInputStream());
					continue;
				} catch (IOException e1) {
					e1.printStackTrace();
					Main.log.e(TAG, "startReceiving()#trying to rebuild in", e.getMessage());
				}
				
				
				disconnect();				//
			}
		}		
	}
	
	@Override
	public synchronized boolean isConnected() {
		return connected;
	}

	@Override
	public void  receivePacket(Packet pckt) {
		if (pckt == null) return;
		
		Main.log.d(TAG, getPlayerName() + " has received " +  pckt.getType() + " packet...");
			
		switch(pckt.getType()){
			/**
			 * CHAT PACKET RECEIVED
			 * forward to server interface if global
			 * forward to match interface if room
			 */
			case CHAT:
				if (pckt.getScope() == SCOPE.GLOBAL){
					Main.log.d(TAG, getPlayerName() + " has received global chat");
					serverConnection.chatReceived(this, ((Chat)pckt).getMessage());
				}else{
					Main.log.d(TAG, getPlayerName() + " has received room chat");
					if (matchConnection != null)
						matchConnection.chatReceived(this, ((Chat)pckt).getMessage());
				}
				break;
				
			/**
			 * REQUESTS	
			 * 
			 */
			case REQUEST:
				switch(((Request) pckt).getReqType()){
					
					/**
					 * ROOM CREATION REQUEST	
					 * host a match using server interface
					 */
					case HOST_MATCH:
						Main.log.d(TAG, getPlayerName() + " has received create room request");
						if (serverConnection != null)
							matchConnection = serverConnection.hostMatch(this, 
									((MatchDetail) pckt).getMatchName());
						break;
					

					/**
					 * JOIN CREATION REQUEST	
					 * get match instance using server interface
					 * then attempt to join it
					 * if @return match.join is null, failed to join
					 */
					case JOIN_MATCH:
						Main.log.d(TAG, getPlayerName() + " has received join room request");
						if (serverConnection != null)
							matchConnection = serverConnection.getMatch(
									((MatchDetail) pckt).getMatchId()).joinMatch(this);
						break;
					
					/**
					 * REFRESH MATCH LIST REQUEST
					 * request fresh list using server interface
					 */
					case REFRESH_ROOMS:
						Main.log.d(TAG, getPlayerName() + " has received refresh room request");
						if (serverConnection != null)
							serverConnection.refreshMatchList(this);
						break;
					
					/**
					 * INITIALIZE PLAYER REQUEST
					 * check if @param name exist on the server.
					 * if not, join/enlist to server using server interface
					 * if exist, send error name already exist
					 */
					case JOIN_SERVER:
						Main.log.d(TAG, getPlayerName() + " has received init name request");
						if (!isNameExist(pckt)) {
							playerName = ((JoinServer) pckt).getPlayerName();
							serverConnection.joinServer(this);
						}
						else sendPacket(Packet.Builder.res.errJoinServerNameExists(Main.senderInetAddress, 
								Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
						break;
						
					case CHANGE_READY:
						if (matchConnection != null)
							matchConnection.toggleReady(this);
						break;
						
					case LEAVE_MATCH:
						if (matchConnection != null)
							matchConnection = matchConnection.leaveMatch(this);
						break;
						
					case START_GAME:
						if (matchConnection != null)
							matchConnection.startMatch(this);
						break;
						
					default:
						break;
						

				}
				break;
				
			/**
			 * RESPONSE OF CLIENT TO SERVER REQUESTS	
			 * currently, none
			 */
			case RESPONSE:
				
				break;
		}
	}
	
	@Override
	public void sendPacket(Packet pckt) {
		if (isConnected()){
			Main.log.d(TAG, "sending " + pckt.getType() + " packet to " + getPlayerName() + "...");
			try {
				out.writeObject(pckt);
				Main.log.d(TAG, "packet " + pckt.getType() + " sent...");
				
			} catch (IOException e) {
				e.printStackTrace();
				Main.log.e(TAG, "sendPacket()", e.getMessage());
				try {
					out = new ObjectOutputStream(socket.getOutputStream());
				} catch (IOException e1) {
					e1.printStackTrace();
					Main.log.e(TAG, "sendPacket()# reinit out", e.getMessage());
					disconnect();
				}
			}
		}else Main.log.i(TAG, "disconnected...");
	}
	
	@Override
	public void disconnect(){		
		synchronized(this){
			connected = false;
		}
		Main.log.d(TAG, "disconnecting...");
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, "disconnect()", e.getMessage());
		}
	}
	
}
