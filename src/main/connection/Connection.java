package main.connection;

import main.packet.Packet;

public interface Connection {
	void startReceiving();
	boolean isConnected();
	void receivePacket(Packet pckt);
	void sendPacket(Packet pckt);
	void disconnect();
}
