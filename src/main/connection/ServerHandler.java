package main.connection;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import main.Main;
import main.ServerConnection;
import main.packet.Packet;

public class ServerHandler extends Thread implements Connection{
	private static final String TAG = ServerHandler.class.getSimpleName();	//For debugging
	private boolean connected;										//For stopping infinite loop
	
	private ServerConnection server;								//for adding players if accepted
	
	private static int port = 3000;									//port for listening
	private ServerSocket serverSocket;								//server socket
	
	/**
	 * SERVER HANDLER CONSTRUCTOR
	 * @param main
	 */
	public ServerHandler(ServerConnection main) {
		this.server = main;
	}
	
	/**
	 * RUN
	 * start accepting
	 */
	@Override
	public void run() {
		Main.log.d(TAG, "starting thread...");
		
		startReceiving();
		
		Main.log.d(TAG, "ending thread...");
	}

	/**
	 * START RECEIVING
	 * initiates: server socket, bind to port PORT
	 * listen and accept clients
	 */
	@Override
	public void startReceiving() {
		connected = true;
		Socket tempSocket;
		ClientHandler tempClient;
		
		while(isConnected()){
			try {
				serverSocket = new ServerSocket(port);
				Main.log.d(TAG, "ServerControl initialized.");
				
				while(true){
					Main.log.d(TAG, "listening at " + serverSocket.getInetAddress().getHostAddress() + ":" + serverSocket.getLocalPort());
					tempSocket = serverSocket.accept();
					
					tempClient = new ClientHandler(server, tempSocket);
					tempClient.start();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
				
				Main.log.e(TAG, e.getMessage());
			}
		}		
	}

	@Override
	public synchronized boolean isConnected() {
		return connected;
	}

	@Override
	public void receivePacket(Packet pckt) {

	}

	@Override
	public void sendPacket(Packet pckt) {

	}

	/**
	 * DISCONNECT
	 * set disconnected
	 * close server socket
	 */
	@Override
	public void disconnect() {
		synchronized(this){
			connected = false;
		}
		Main.log.d(TAG, "stopping accept...");
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, "stopAccepting()", e.getMessage());
		}		
	}
}
