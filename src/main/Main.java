package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import main.connection.ClientHandler;
import main.connection.ServerHandler;
import main.logger.Logger;
import main.match.Match;
import main.match.MatchConnection;
import main.packet.Packet;
import main.packet.Packet.SCOPE;

public final class Main implements ServerControl, ServerConnection{
	private static final String TAG = Main.class.getSimpleName();			//for debugging
	public static Logger log;							//for logging
	
	private static int NEXT_MATCH_ID = 0;				//for assigning IDs to matches
	public static Ui ui;								//for UI 
	private ServerHandler serverHandler;				//Connection acceptor thread
	
	private ArrayList<ClientHandler> onlinePlayers;		//Players joined
	private Map<Integer, Match> matches;				//matchID <=> match

	public static String senderInetAddress;
	public static String receiverInetAddress;
	public static int senderPort;
	public static int receiverPort;
	
	/**
	 * MAIN CONSTRUCTOR
	 * initiates: user interface, logger
	 * starts: server connection acceptor
	 */
	public Main() {
		ui = new Ui();
		log = new Logger(ui); 
		
		startServer();
		
		log.d(TAG, "starting...");
	}
	
	
	/**
	 * SERVER CONTROLS ------------------------------------------------------------------
	 * JOIN SERVER
	 * adds @param player to list of online players
	 * should be called if player name is accepted
	 * informs @param player that his/her name is accepted
	 */
	@Override
	public synchronized void joinServer(ClientHandler player) {
		if (onlinePlayers != null){
			onlinePlayers.add(player);
			
			player.sendPacket(Packet.Builder.res.successServerJoin(player.getName(), 
					senderInetAddress, receiverInetAddress, senderPort, receiverPort));
			
			log.i(TAG, player.getPlayerName() + " has joined the server.");
			
			//TODO: improve UI
			ui.addOnlinePlayer(player.getPlayerName());
			
		}else log.w(TAG, "uninitialized onlinePlayers");
	}

	/**
	 * LEAVE SERVER
	 * removes player @param player to list of online players
	 * should be last call before ending client thread
	 */
	@Override
	public synchronized void leaveServer(ClientHandler player) {
		if (onlinePlayers != null){
			if (onlinePlayers.contains(player)){
				
				onlinePlayers.remove(player);
				log.i(TAG, player.getPlayerName() + " has left the server.");
				
				//TODO: improve UI
				ui.removeOnlinePlayer(player.getPlayerName());
			}
		}else log.w(TAG, "uninitialized onlinePlayers");	
	}

	
	/**
	 * MATCH CONTROLS ------------------------------------------------------------------
	 * HOST MATCH
	 * initializes: new match with @param player as host
	 * broadcast to other players new room's details
	 */
	@Override
	public MatchConnection hostMatch(ClientHandler player, 
			String matchName) {
		if (matches != null){
			Match temp = new Match(this, matchName, NEXT_MATCH_ID++, player);
			
			matches.put(temp.getMatchID(), temp);
			
			sendToAll(player, Packet.Builder.res.refreshListMatch(temp.getMatchID(), temp.getMatchName(), 0, 0, temp.getHost().getPlayerName(),
						1, true, senderInetAddress, receiverInetAddress, senderPort, receiverPort));
			
			//TODO: improve UI
			ui.addOnlinePlayer("Match " + temp.getMatchName());
			
			return temp;
		}else log.w(TAG, "uninitialized matches");
		return null;
	}
	
	/**
	 * REMOVE MATCH
	 * removes a match
	 * called after last player within room leaves/disconnects
	 * sends notice to other players that room has been destroyed
	 * 
	 * @param matchID
	 * @param matchName
	 * @param hostID
	 * @param hostName
	 */
	public synchronized void removeMatch(int matchID, 
			String matchName, int hostID, String hostName){
		if (matches != null){
			if (matches.containsKey(matchID)){
				matches.remove(matchID);
				log.i(TAG, matchName + " has been destroyed.");
			
				sendToAll(null, Packet.Builder.res.refreshListMatch(matchID, matchName, 0, 0, hostName,
						0, false, senderInetAddress, receiverInetAddress, senderPort, receiverPort));
				
				//TODO: improve UI
				ui.removeOnlinePlayer("Match " + matchName);
			}
		}else log.w(TAG, "uninitialized matches");
	}

	/**
	 * GET MATCH
	 * gets match with match number @param matchID
	 * for joining
	 */
	@Override
	public synchronized Match getMatch(int matchID) {
		if (matches != null){
			if (matches.containsKey(matchID)){
				return matches.get(matchID);
			}
		}else log.w(TAG, "uninitialized matches");
		return null;
	}
	
	
	/**
	 * SERVER SWITCH ---------------------------------------------------------------------
	 * REFRESH MATCH LIST
	 * sends: list of match details to
	 * @param player
	 */
	@Override
	public synchronized void refreshMatchList(ClientHandler player) {
		log.i(TAG, player.getPlayerName() + " has requested a refresh...");
		if (matches != null){
			if (matches.values().size() > 1) log.i(TAG, "Sending match details to " + player.getPlayerName());
			else log.i(TAG, player.getPlayerName() + "No matches to send...");
			
			for(Match mt: matches.values()){
				player.sendPacket(Packet.Builder.res.refreshListMatch(mt.getMatchID(), mt.getMatchName(), 0, 0, mt.getHost().getPlayerName(),
						mt.numOfPlayers(), true, senderInetAddress, receiverInetAddress, senderPort, receiverPort));
			}
			
			if (matches.values().size() > 1) log.i(TAG, "Finished Sending  match details to " + player.getPlayerName());
		}else log.w(TAG, "uninitialized matches");
	}

	/**
	 * CHAT RECEIVED
	 * a global chat has been received
	 * send message to rest of players except sender
	 */
	@Override
	public void  chatReceived(ClientHandler player, String message) {
		if (onlinePlayers != null){
			sendToAll(player, Packet.Builder.cht.chat(SCOPE.GLOBAL, player.getPlayerName(), 
					message, senderInetAddress, receiverInetAddress, senderPort, receiverPort));
			
			//TODO: fix UI
			ui.addChatMessage(player.getPlayerName(), message);
		}else log.w(TAG, "uninitialized onlinePlayers");	
	}

	/**
	 * IS NAME EXIST
	 * checks if @param name exists
	 */
	@Override
	public synchronized boolean isNameExist(String name) {
		if (onlinePlayers != null){
			for(ClientHandler cl:onlinePlayers){
				if (cl.getPlayerName().equalsIgnoreCase(name))
					return true;
			}
		}else log.w(TAG, "uninitialized onlinePlayers");	
		return false;
	}
	
	/**
	 * Sends a packet @param pckt to all online players 
	 * except @param exceptPlayer
	 */
	private void sendToAll(ClientHandler exceptPlayer, Packet pckt){
		if (onlinePlayers != null){
			Main.log.i(TAG, "broadcasting to all players packet " + pckt.getType());
			
			for(ClientHandler cl:onlinePlayers){
				if (!cl.equals(exceptPlayer)){
					cl.sendPacket(pckt);
				}
			}
			
			Main.log.i(TAG, "Finished broadcasting to all players in server");
		}else Main.log.w(TAG, "uninitialized onlinePlayers");
	}

	
	/**
	 * SERVER SWITCH ---------------------------------------------------------------------
	 * STOP SERVER
	 * stops: server acceptor thread
	 * nullifies: onlinePlayers, matches, server acceptor thread
	 */
	@Override
	public synchronized void stopServer() {
		serverHandler.disconnect();	
		
		onlinePlayers = null;
		matches = null;
		serverHandler = null;
	}

	/**
	 * START SERVER
	 * initiates: list of onlinePlayers, matches, server acceptor thread
	 * starts: server acceptor thread
	 */
	@Override
	public synchronized void startServer() {
		onlinePlayers = new ArrayList<ClientHandler>();
		matches = new HashMap<Integer, Match>();
		serverHandler = new ServerHandler(this);
		
		serverHandler.start();
		
		//TODO: Stop server if port already bound?
	}
	
	
}
