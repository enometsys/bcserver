package main.match;

import main.connection.ClientHandler;

public interface MatchConnection {
	void chatReceived(ClientHandler player, String message);
	MatchConnection leaveMatch(ClientHandler player);
	void startMatch(ClientHandler player);
	
	void toggleReady(ClientHandler player);
	
	void matchStarted(ClientHandler player);
	void updateEveryone(ClientHandler callingPlayer);
	String getMatchName();
}
