package main.match;

import java.util.HashMap;
import java.util.Map;

import main.Main;
import main.connection.ClientHandler;
import main.packet.Packet;
import main.packet.Packet.SCOPE;

public class Match implements MatchConnection{
	private static final String TAG = Match.class.getSimpleName();		//For debugging
	
	private static final int MAX_PLAYERS = 5;							//MAX PLAYERS allowed to join
	private int NEXT_PLAYER_ID = 1;											//next id of next player to join
	private Main main;													//for removing match to list when destroyed
	
	/**
	 * MATCH's DETAILS:
	 * 
	 * matchName	<- name of match
	 * matchID		<- id of match
	 * host			<- current host of match
	 * hasStarted	<- if game has already started
	 * mapID		<- current map TODO: make changing map possible
	 */
	private String matchName;
	private int matchID;
	private ClientHandler host;
	private boolean hasStarted;
	private int mapID = 0;
	
	
	/**
	 * PLAYERS' DETAILS
	 * 
	 * players' ready state
	 * players' id
	 * players' mapLocations
	 * if players' has started their game
	 */
	private Map<ClientHandler, Boolean> players;			//Player <=> ready state
	private Map<ClientHandler, Integer> playerIDs;			//Player <=> playerID
	private Map<ClientHandler, Integer> playerMapLocations;	//player <=> mapLocation
	private Map<ClientHandler, Boolean> playerStartState;	//player <=> started
	
	private String udpServerInet;
	private int updServerPort;
	/**
	 * MATCH CONSTRUCTOR
	 * initialize: maps of readyStates, iDs, map locations, start state
	 * @param main
	 * @param matchName
	 * @param matchID		
	 * @param host
	 */
	public Match(Main main, String matchName, int matchID, ClientHandler host) {
		this.main = main;
		this.matchName = matchName;
		this.matchID = matchID;
		this.host = host;
		
		players = new HashMap<ClientHandler, Boolean>();
		playerIDs = new HashMap<ClientHandler, Integer>();
		playerMapLocations = new HashMap<ClientHandler, Integer>();
		playerStartState = new HashMap<ClientHandler, Boolean>();
		
		joinMatch(host, true);
		
		Main.log.d(TAG, matchID + " " + matchName + " hosted by " + host.getPlayerName() + " created.");
	}
	
	public synchronized MatchConnection joinMatch(ClientHandler player){
		return joinMatch(player, false);
	}
	
	/**
	 * JOIN MATCH
	 * @param player has attempted to join match
	 * sends error if match already started
	 * sends error if no slot available
	 * @return instance if @param player already is a member
	 * register @param player to match players
	 * send to @param player successful join packet
	 * @return instance after joining
	 */
	public synchronized MatchConnection joinMatch(ClientHandler player, boolean isReady){
		if (hasStarted){
			//send error to player, match already started
			Main.log.w(TAG, "Match " + matchName + "already started");
			player.sendPacket(Packet.Builder.res.errJoinMatchDNE(
					Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
			return null;
		}
		
		//player is already a member of room, for reconnecting? Retrieving lost connection?
		if (players.containsKey(player)) return this;
		
		if (numOfPlayers() < MAX_PLAYERS){
			//player slot available
			Main.log.d(TAG, player.getPlayerName() + " trying to join match " + 
					matchName + " hosted by " + host.getPlayerName());
			
			players.put(player, isReady);
			playerIDs.put(player, NEXT_PLAYER_ID++);
			playerMapLocations.put(player, -1);
			playerStartState.put(player, false);
			
			//send player his playerID for the match
			player.sendPacket(Packet.Builder.res.successJoinMatch(playerIDs.get(player), getMatchID(), getMatchName(), mapID, 
								playerIDs.get(host), host.getPlayerName(), numOfPlayers(), 
								Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
			
			
			Main.log.d(TAG, player.getPlayerName() + " has joined match " + 
					matchName + " hosted by " + host.getPlayerName());
			
			updateEveryone(player);
			
			return this;
		}else{
			//send error to player, match full
			Main.log.w(TAG, "Match " + matchName + "already full");
			player.sendPacket(Packet.Builder.res.errJoinMatchFull(Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
			return null;
		}
	}
	
	/**
	 * LEAVE MATCH
	 * @param player has left the match
	 * remove from list
	 * notify others of @param player leaving
	 * 
	 * if there are other players
	 * 		if @param player is host, choose another, return
	 * 		else return
	 * 
	 * destroyMatch
	 * unregister match to server list
	 */
	@Override
	public synchronized MatchConnection leaveMatch(ClientHandler player) {
		Main.log.w(TAG, player.getPlayerName() + 
				" leaving match " + getMatchName() + "...");
		
		int 	tempID 			= playerIDs.get(player);
		String 	tempName 		= player.getPlayerName();
		int 	tempMapLocation = playerMapLocations.get(player);
		boolean tempReady 		= players.get(player);
		boolean tempStarted 	= playerStartState.get(player);
		
		players.remove(player);
		playerIDs.remove(player);
		playerMapLocations.remove(player);
		playerStartState.remove(player);
		
		if (host.equals(player)) changeHost(player);		//if host
				
		Main.log.d(TAG, player.getPlayerName() + " has left match " + matchName + " report to others...");	//report to everyone
		
		//Notify everyone of player leaving
		sendToAll(player, Packet.Builder.res.playerDetail(getMatchID(), getMatchName(), getMapID(), 
				playerIDs.get(host), host.getPlayerName(), numOfPlayers(), true, 
				tempMapLocation, tempID, tempName, tempReady, tempStarted, 
				Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
		
		updateEveryone(player);		
		
		if (players.size() > 0) return null;
		
		main.removeMatch(matchID, matchName, tempID, tempName);
		return null;
		
	}

	/**
	 * START MATCH
	 * host has started the match
	 * sends error if others aside from host starts game
	 * sends error if not everyone is ready yet
	 * sent start packets to other players
	 */
	@Override
	public synchronized void startMatch(ClientHandler player) {
		if (host.equals(player)){
			Main.log.d(TAG, "Host " + player.getPlayerName() + " has requested to start match " + matchName);
			
			if (players.values().contains(false)){
				//Not everyone is ready yet, send warning to host
				Main.log.e(TAG, "Error to Host " + player.getPlayerName() + ", not everyone ready in match " + matchName);
				player.sendPacket(Packet.Builder.res.errStartGameNotEveryoneReady(
						Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
			}else{
				//everyone is ready, send start signal to everyone
				Main.log.i(TAG, host.getPlayerName() + " has started match " + matchName);
				playerStartState.put(player, true);
				
				//TODO: assign map location to all, position chosen by player?
				//temporary solution: increasing order
				Main.log.i(TAG, "Assigning map locations at match " + matchName);
				int ind = 0;
				for(ClientHandler cl:players.keySet()){
					playerMapLocations.put(cl, ind++);
				}
				
				//TODO: notify players of their given assigned map locations
				updateEveryone(player);
				Main.log.i(TAG, "Finished Assigning map locations at match " + matchName);
				
				initUDPServer();
				
				sendToAll(null, Packet.Builder.res.startGame(getMatchID(), getMatchName(), getMapID(), getHostId(), host.getPlayerName(), numOfPlayers(), true, 
						udpServerInet, updServerPort, Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
				hasStarted = true;
			}
		}else{
			Main.log.w(TAG, "Non host " + player.getPlayerName() + 
					" has attempted to start match " + matchName + " hosted by " + host.getPlayerName());
			player.sendPacket(Packet.Builder.res.errStartGameNotHost(
					Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
		}
		
	}
	
	private void initUDPServer(){
		udpServerInet = Main.senderInetAddress;
		updServerPort = 3000;
	}
	
	/**
	 * TOGGLE READY
	 * @param player has change ready status
	 */
	@Override
	public synchronized void toggleReady(ClientHandler player) {
		//toggle ready state
		Main.log.i(TAG, player.getPlayerName() + " has toggled his/her ready state");
		
		if (host.equals(player)) players.put(host, true);
		else players.put(player, !players.get(player));		
		
		updateEveryone(player);
	}

	/**
	 * MATCH STARTED
	 * @param player has started his/her game
	 */
	@Override
	public synchronized void matchStarted(ClientHandler player) {
		//Player has started his/her game, report to others
		Main.log.i(TAG, player.getPlayerName() + " has started his/her game");
		
		playerStartState.put(player, true);
		
		updateEveryone(player);
	}
	
	/**
	 * UPDATE EVERYONE
	 * updates every player within about latest room state
	 * i.e., room details, player details
	 */
	@Override
	public void updateEveryone(ClientHandler callingPlayer) {
		for(ClientHandler cl:players.keySet()){
			sendToAll(null,Packet.Builder.res.playerDetail(getMatchID(), getMatchName(), getMapID(), 
					playerIDs.get(host), host.getPlayerName(), numOfPlayers(), false, 
					playerMapLocations.get(cl), playerIDs.get(cl), cl.getPlayerName(), players.get(cl), playerStartState.get(cl), 
					Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));
			
			
		}
	}
	
	private synchronized void sendToAll(ClientHandler exceptPlayer, Packet pckt){
		if (players != null){
			Main.log.i(TAG, "broadcasting to all players in match " + getMatchName() + " packet " + pckt.getType());
			
			for(ClientHandler cl:players.keySet()){
				if (!cl.equals(exceptPlayer)){
					cl.sendPacket(pckt);
				}
			}
			
			Main.log.i(TAG, "Finished broadcasting to all players in match " + getMatchName());
		}else Main.log.w(TAG, "uninitialized players");
	}

	@Override
	public void chatReceived(ClientHandler player, String message) {
		sendToAll(player, Packet.Builder.cht.chat(SCOPE.ROOM, player.getPlayerName(), message,
				Main.senderInetAddress, Main.receiverInetAddress, Main.senderPort, Main.receiverPort));	
	}

	private void changeHost(ClientHandler callingPlayer) {
		Main.log.i(TAG, "Host " + callingPlayer.getPlayerName() + " has left match " + getMatchName());
		
		for(ClientHandler cl:players.keySet()){
			//choose first to be selected by forEach
			host = cl;
			players.put(cl, true);
			Main.log.i(TAG, "Making " + cl.getPlayerName() + " the next host...");
			
			updateEveryone(callingPlayer);
			return;
		}
	}
	
	@Override
	public String getMatchName() {
		return matchName;
	}

	public int getMatchID() {
		return matchID;
	}

	public synchronized boolean hasStarted() {
		return hasStarted;
	}

	public synchronized ClientHandler getHost() {
		return host;
	}
	
	public synchronized int getHostId(){
		if (host != null)
			return playerIDs.get(host);
		else return -1;
	}
	
	public synchronized int numOfPlayers(){
		return players.size();
	}

	public int getMapID() {
		return mapID;
	}

}
