package main;

import main.connection.ClientHandler;
import main.match.Match;
import main.match.MatchConnection;

public interface ServerConnection {
	void joinServer(ClientHandler player);
	void leaveServer(ClientHandler player);
	
	MatchConnection hostMatch(ClientHandler player, String matchName);
	Match getMatch(int matchID);
	
	void refreshMatchList(ClientHandler player);
	void chatReceived(ClientHandler player, String message);
	boolean isNameExist(String name);
}
