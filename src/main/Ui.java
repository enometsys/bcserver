package main;

import java.awt.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

public class Ui {

	private JFrame frame;
	private List lstOnlinePlayers;
	private List lstChat;
	private List lstLogs;
	/**
	 * Create the application.
	 */
	public Ui() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Battle Clans Server");
		frame.setBounds(100, 100, 605, 484);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new MigLayout("", "[grow][grow]", "[][][][grow][][grow]"));
		
		JLabel lblNewLabel = new JLabel("Online Players");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblNewLabel, "cell 0 0 1 2,alignx center,aligny center");
		
		JLabel lblChatMessages = new JLabel("Chat Messages");
		lblChatMessages.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblChatMessages, "cell 1 1,alignx center,aligny center");
		
		lstOnlinePlayers = new List();
		frame.getContentPane().add(lstOnlinePlayers, "cell 0 2 1 4,grow");
		
		lstChat = new List();
		frame.getContentPane().add(lstChat, "cell 1 2 1 2,grow");
		
		JLabel lblLogs = new JLabel("Logs");
		frame.getContentPane().add(lblLogs, "cell 1 4,alignx center,aligny center");
		
		lstLogs = new List();
		frame.getContentPane().add(lstLogs, "cell 1 5,grow");
	}
	
	
	
	public void addOnlinePlayer(String name){
		lstOnlinePlayers.add(name);
	}
	
	public void removeOnlinePlayer(String name){
		lstOnlinePlayers.remove(name);
	}
	
	public void addChatMessage(String from, String message){
		lstChat.add(from + ": " + message);
	}
	
	public void addToLogs(String log){
		lstLogs.add(log);
	}
}
