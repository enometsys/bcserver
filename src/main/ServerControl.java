package main;


public interface ServerControl {
	void stopServer();
	void startServer();
}
