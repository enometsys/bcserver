package main.logger;

import main.Ui;

public class Logger {
	private Ui ui;
	
	public Logger(Ui ui) {
		this.ui = ui;
	}
	
	public Logger() {
		
	}



	/**
	 * For Verbose
	 * @param TAG
	 * @param message
	 */
	public void v(String TAG, String message){
		tempWrite("Verbose", TAG, null, message);
	}
	
	/**
	 * For Debugging
	 * @param TAG
	 * @param message
	 */
	public void d(String TAG, String message){
		tempWrite("Debug", TAG, null,message);
	}
	
	/**
	 * For Errors
	 * @param TAG
	 * @param message
	 */
	public void e(String TAG, String message){
		tempWrite("Error", TAG, null, message);
	}
	
	public void e(String TAG, String method, String message){
		tempWrite("Error", TAG, method, message);
	}
	
	/**
	 * For Warnings
	 * @param TAG
	 * @param message
	 */
	public void w(String TAG, String message){
		tempWrite("Warning", TAG, null, message);
	}
	
	public void w(String TAG, String method, String message){
		tempWrite("Warning", TAG, method, message);
	}
	
	/**
	 * For Information
	 * @param TAG
	 * @param message
	 */
	public void i(String TAG, String message){
		tempWrite("Information", TAG, null, message);
	}
	
	public void i(String TAG, String method,String message){
		tempWrite("Information", TAG, method, message);
	}
	
	public void tempWrite(String what, String TAG, String method, String message){
		String temp = (method == null)? "": ("on method " + method + "...");
		//System.out.println(what + " " + TAG + " : " + temp + " " + message);
		ui.addToLogs(what + " " + TAG + " : " + temp + " " + message);
	}
}
